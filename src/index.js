/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import styles from './styles';

import AppNavigator from './routes';

type Props = {};

const App = () => {
  return (
    <Fragment>
      <AppNavigator />
    </Fragment>
  )
}

export default App;
