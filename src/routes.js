import { createStackNavigator, createAppContainer } from "react-navigation";

import Home from './pages/home';
import Button from './pages/button';
import Picker from './pages/picker';
import Slider from './pages/slider';
import Switch from './pages/switch';

const AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
  },
  Button: {
    screen: Button,
  },
  Picker: {
    screen: Picker,
  },
  Slider: {
    screen: Slider,
  },
  Switch: {
    screen: Switch,
  },
});

export default createAppContainer(AppNavigator);
