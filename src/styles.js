import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },
  componentsContainer: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  componentsTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingBottom: 50,
  },
  buttonContainer: {
    backgroundColor: '#FFA07A',
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 15,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '700',
  },
});

export default styles;
