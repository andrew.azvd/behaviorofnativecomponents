import React, { Component } from 'react';
import { TouchableOpacity, View, Text, Picker } from 'react-native';

import styles from '../../styles';

const values = [
  'Java',
  'JavaScript',
  'PHP',
];

class PickerContainer extends Component {

  state = {
    language: 'js',
  }

  render() {
    return (
      <View style={styles.componentsContainer}>
        <Text style={styles.componentsTitle}>Picker Component</Text>

        <Picker
          selectedValue={this.state.language}
          style={{ height: 50, width: 100 }}
          onValueChange={(itemValue, itemIndex) => this.setState({ language: itemValue })}
        >
          <Picker.Item label="Java" value="java" />
          <Picker.Item label="JavaScript" value="js" />
          <Picker.Item label="C#" value="c#" />
          <Picker.Item label="PHP" value="php" />
          <Picker.Item label="Ruby" value="ruby" />
        </Picker>
      </View>
    )
  }
}

export default PickerContainer;
