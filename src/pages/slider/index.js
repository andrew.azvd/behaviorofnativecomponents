import React, { Component } from 'react';
import { TouchableOpacity, View, Text, Slider } from 'react-native';

import styles from '../../styles';

const values = [
  'Java',
  'JavaScript',
  'PHP',
];

class SliderContainer extends Component {

  state = {
    language: 'js',
  }

  render() {
    return (
      <View style={styles.componentsContainer}>
        <Text style={styles.componentsTitle}>Picker Component</Text>

        <Slider
           style={{ width: 300 }}
           step={1}
           minimumValue={18}
           maximumValue={71}
           value={this.state.age}
           onValueChange={val => console.log(val)}
        />
      </View>
    )
  }
}

export default SliderContainer;
