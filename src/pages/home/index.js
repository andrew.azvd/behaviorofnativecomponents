import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

import styles from './styles';
import commonStyles from '../../styles';

const data = [
  { id: 1, label: 'Button' },
  { id: 2, label: 'Picker' },
  { id: 3, label: 'Slider' },
  { id: 4, label: 'Switch' },
  { id: 5, label: 'Flatlist' },
  { id: 6, label: 'SectionList' },
  { id: 7, label: 'ActivityIndicator' },
  { id: 8, label: 'Animated' },
  { id: 9, label: 'Modal' },
  { id: 10, label: 'WebView' },
  { id: 11, label: 'DatePickerIOS' },
  { id: 12, label: 'Drawer' },
  { id: 13, label: 'ProgressBar' },
  { id: 14, label: 'TabBar' },
]

const Item = ({ label, onPress }) => (
  <TouchableOpacity style={commonStyles.buttonContainer} onPress={onPress}>
    <Text style={commonStyles.buttonText}>
      {label}
    </Text>
  </TouchableOpacity>
)

class Home extends Component {
  static navigationOptions = {
    title: 'NATIVE COMPONENTS',
  };

  render() {
    return (
      <FlatList
        style={styles.container}
        data={data}
        renderItem={({ item }) => <Item label={item.label} onPress={() => this.props.navigation.navigate(item.label)} />}
      />
    )
  }
}

export default Home;
