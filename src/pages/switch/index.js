import React, { Component } from 'react';
import { View, Text, Switch } from 'react-native';

import styles from '../../styles';

class SwitchComponent extends Component {
  state = {
    active: false,
  }

  toggleSwitch = () => {
    this.setState({ active: !this.state.active });
  }

  render() {
    return (
      <View style={styles.componentsContainer}>
        <Text style={styles.componentsTitle}>Picker Component</Text>

        <Switch
          onValueChange={this.toggleSwitch}
          value={this.state.active}
          thumbColor="#FF4500"
          trackColor="#FF6347"
        />
      </View>
    )
  }
}

export default SwitchComponent;
