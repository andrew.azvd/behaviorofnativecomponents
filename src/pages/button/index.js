import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

import styles from '../../styles';

const ButtonContainer = () => {
  return (
    <View style={styles.componentsContainer}>
      <Text style={styles.componentsTitle}>Button Component</Text>

      <TouchableOpacity
        onPress={() => null}
        style={styles.buttonContainer}
      >
        <Text style={styles.buttonText}>Click me!</Text>
      </TouchableOpacity>
    </View>
  )
}

export default ButtonContainer;
